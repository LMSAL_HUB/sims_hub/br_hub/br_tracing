"""
Created by Abinaya  (Daniel Nobrega Siverio)
Set of programs to read and interact with output from BifrostData
simulations focusing on tracing particles (Lagrange)
"""
import numpy as np
import os
from helita.sim.bifrost import BifrostData as br
from helita.sim.bifrost import Bifrost_units as br_u
import imp
import scipy as sp


class Lagrange_tracing(br):
    """
    Class that operates radiative transfer from BifrostData simulations
    in native format.
    """

    def __init__(self, verbose=False, *args, **kwargs):
        if verbose:
            print(kwargs)

        super(Lagrange_tracing, self).__init__(*args, **kwargs)
        """
        sets all stored vars to None
        """
        self.verbose = verbose  # for testing, True --> printouts
        self.units = br_u()

    def new_function(self):
        '''
        Add description here
        '''
